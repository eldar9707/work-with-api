<?php

namespace Tests;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase, WithFaker;

    /**
     * @var Collection|Model
     */
    protected $articles, $user, $article, $comments, $comment;


    /**
     * SetUp
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->count(5)->for($this->user)->create();
        $this->article = $this->articles->random();
        $this->comments = Comment::factory()->count(5)->create([
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
            'body' => $this->faker->paragraph,
        ]);
        $this->comment = $this->comments->random();
    }
}

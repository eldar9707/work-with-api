<?php

namespace Tests\Feature;

use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * Success show comments
     *
     * @group comments
     * @return void
     */
    public function test_success_get_all_comments()
    {
        $request = $this->getJson(route('comments.index'));
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->comments->count(), $payload->data);
    }


    /**
     * Success get comment.
     * @group comments
     * @return void
     */
    public function test_success_get_comment()
    {
        Passport::actingAs(
            $this->user,
            [route('comments.show', ['comment' => $this->comment])]
        );
        $request = $this->getJson(route('comments.show', ['comment' => $this->comment]));
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($this->comment->id, $payload->data->id);
    }


    /**
     * Failed get comment.
     * @group comments
     * @return void
     */
    public function test_failed_get_comment()
    {
        $request = $this->getJson(route('comments.show', ['comment' => $this->comment]));
        $request->assertUnauthorized();
    }


    /**
     * Success create comment.
     * @group comments
     * @return void
     */
    public function test_success_create_comment()
    {
        Passport::actingAs(
            $this->user,
            [route('comments.show', ['comment' => $this->comment])]
        );

        $data = [
            'body' => $this->faker->paragraph(4),
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $request = $this->postJson(route('comments.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('comments', $data);

        $this->assertDatabaseHas('comments', ['id' => $payload->data->id]);
    }


    /**
     * Failed create comment.
     * @group comments
     * @return void
     */
    public function test_failed_create_comment()
    {
        $data = [
            'body' => $this->faker->paragraph(4),
            'user_id' => $this->user->id,
            'article_id' => $this->article->id,
        ];
        $request = $this->postJson(route('comments.store'), $data);
        $request->assertUnauthorized();
    }


    /**
     * Success delete comment.
     * @group comments
     * @return void
     */
    public function test_success_delete_comment()
    {
        Passport::actingAs(
            $this->user,
            [route('comments.destroy', ['comment' => $this->comment])]
        );
        $request = $this->deleteJson(route('comments.destroy', ['comment' => $this->comment]));
        $request->assertStatus(204);
        $payload = json_decode($request->getContent());
        $this->assertNull($payload);
    }

    /**
     * Success delete comment.
     * @group comments
     * @return void
     */
    public function test_failed_delete_comment()
    {
        $request = $this->deleteJson(route('comments.destroy', ['comment' => $this->comment]));
        $request->assertUnauthorized();
    }

    /**
     * Success update comment.
     * @group comments
     * @return void
     */
    public function test_success_update_comment()
    {
        Passport::actingAs(
            $this->user,
            [route('comments.update', ['comment' => $this->comment])]
        );

        $data = [
            'article_id' => $this->comment->article_id,
            'user_id' => $this->comment->user_id,
            'body' => $this->faker->paragraph,
        ];

        $request = $this->putJson(route('comments.update', ['comment' => $this->comment]), $data);
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertEquals($payload->data->body, $data['body']);
    }


    /**
     * Failed update comment.
     * @group comments
     * @return void
     */
    public function test_failed_update_comment()
    {
        $data = [
            'article_id' => $this->comment->article_id,
            'user_id' => $this->comment->user_id,
            'body' => $this->faker->paragraph,
        ];

        $request = $this->putJson(route('comments.update', ['comment' => $this->comment]), $data);
        $request->assertUnauthorized();
    }
}

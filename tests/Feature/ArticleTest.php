<?php

namespace Tests\Feature;

use Laravel\Passport\Passport;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * Success get all articles.
     * @group articles
     * @return void
     */
    public function test_can_get_all_articles()
    {
        $request = $this->getJson(route('articles.index'));
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->articles->count(), $payload->data);
    }


    /**
     * Success get article.
     * @group articles
     * @return void
     */
    public function test_success_get_article()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.show', ['article' => $this->article])]
        );
        $request = $this->getJson(route('articles.show', ['article' => $this->article]));
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($this->article->id, $payload->data->id);
    }


    /**
     * Failed get article.
     * @group articles
     * @return void
     */
    public function test_failed_get_article()
    {
        $request = $this->getJson(route('articles.show', ['article' => $this->article]));
        $request->assertUnauthorized();
    }


    /**
     * Success create article.
     * @group articles
     * @return void
     */
    public function test_success_create_article()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.show', ['article' => $this->article])]
        );

        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('articles', $data);

        $this->assertDatabaseHas('articles', ['id' => $payload->data->id]);
    }


    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_failed_create_article()
    {
        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $request->assertUnauthorized();
    }

    /**
     * Success delete article.
     * @group articles
     * @return void
     */
    public function test_success_delete_article()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.destroy', ['article' => $this->article])]
        );
        $request = $this->deleteJson(route('articles.destroy', ['article' => $this->article]));
        $request->assertStatus(204);
        $payload = json_decode($request->getContent());
        $this->assertNull($payload);
    }

    /**
     * Success delete article.
     * @group articles
     * @return void
     */
    public function test_failed_delete_article()
    {
        $request = $this->deleteJson(route('articles.destroy', ['article' => $this->article]));
        $request->assertUnauthorized();
    }

    /**
     * Success create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.store', ['article' => $this->article])]
        );

        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }


    /**
     * Success create article.
     * @group articles
     * @return void
     */
    public function test_validation_error()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.store', ['article' => $this->article])]
        );

        $data = [];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }


    /**
     * Success update article.
     * @group articles
     * @return void
     */
    public function test_success_update_article()
    {
        Passport::actingAs(
            $this->user,
            [route('articles.update', ['article' => $this->article])]
        );

        $data = [
            'user_id' => $this->article->user_id,
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];

        $request = $this->putJson(route('articles.update', ['article' => $this->article]), $data);
        $request->assertOk();
        $payload = json_decode($request->getContent());
        $this->assertEquals($payload->data->title, $data['title']);
    }


    /**
     * Failed update article.
     * @group articles
     * @return void
     */
    public function test_failed_update_article()
    {
        $data = [
            'user_id' => $this->article->user_id,
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
        ];

        $request = $this->putJson(route('articles.update', ['article' => $this->article]), $data);
        $request->assertUnauthorized();
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'body' => ['bail', 'required', 'min:2', 'string'],
            'user_id' => ['bail', 'required', 'exists:App\Models\User,id'],
            'article_id' => ['bail', 'required', 'exists:App\Models\Article,id'],
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $articles = Article::all();
        for ($i = 0; $i < 10; $i++) {
            Comment::factory()->count(rand(1, 4))
                ->for($users->random())
                ->for($articles->random())
                ->create();
        }
    }
}

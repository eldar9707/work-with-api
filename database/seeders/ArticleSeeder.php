<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        for ($i = 0; $i < 10; $i++) {
            Article::factory()->count(rand(1, 3))->for(
                $users->random()
            )->create();
        }
    }
}
